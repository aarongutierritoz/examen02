package cql.ecci.ucr.ac.examen02;

import android.content.Context;
import android.content.Intent;

import androidx.test.espresso.intent.rule.IntentsTestRule;
import androidx.test.platform.app.InstrumentationRegistry;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onData;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.swipeUp;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static org.hamcrest.Matchers.anything;
import static org.junit.Assert.*;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {
    @Rule
    public IntentsTestRule<MainActivity> intentsTestRule = new IntentsTestRule<>(MainActivity.class, true, false);;
    @Test
    public void useAppContext() {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getInstrumentation().getTargetContext();
        assertEquals("cql.ecci.ucr.ac.examen02", appContext.getPackageName());
    }

    @Test
    public void touchItemList() {
        Intent intent = new Intent();
        intentsTestRule.launchActivity(intent);

        for (int i = 0; i<8;++i){
            onData(anything()).inAdapterView(withId(R.id.list))
                    .atPosition(i)
                    .perform(click());
        }

    }
    @Test
    public void scrollList() {
        Intent intent = new Intent();
        intentsTestRule.launchActivity(intent);

        onData(anything()).inAdapterView(withId(R.id.list))
                .atPosition(0)
                .perform(swipeUp());

    }

}