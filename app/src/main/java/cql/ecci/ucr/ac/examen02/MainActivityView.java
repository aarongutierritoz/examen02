package cql.ecci.ucr.ac.examen02;

import java.util.List;

public interface MainActivityView {
    // Mostrar el progreso en la UI del avance de la tarea a realizar
    void showProgress();

    // Esconder el indicador de progreso de la UI
    void hideProgress();

    // Mostrar los items de la lista en la UI
    void setItems(List<TableTop> items);

    // Mostrar mensaje en la UI
    void showMessage(String message);
}
