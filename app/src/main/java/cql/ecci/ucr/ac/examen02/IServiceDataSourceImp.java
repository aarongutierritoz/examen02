package cql.ecci.ucr.ac.examen02;

import android.content.Context;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class IServiceDataSourceImp  implements  IServiceDataSource{
    TableTop tableTop;

    public IServiceDataSourceImp() {
        tableTop = new TableTop();
    }

    @Override
    public List<TableTop> obtainItems(Context context) throws BaseDataItemsException {
        List<TableTop> items = null;

        try {

            // TODO: Obtener de la base de datos
            items = createArrayList(context);

        } catch (Exception e) {
            throw new BaseDataItemsException(e.getMessage());
        }

        return items;
    }

    private List<TableTop> createArrayList(Context context) throws IOException, ExecutionException, InterruptedException {
        tableTop.leer(context);
        return tableTop.getFileName();
    }
}
