package cql.ecci.ucr.ac.examen02;

import android.os.AsyncTask;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

public class AsyncRetrieverImp extends AsyncTask<Object, Void, Void> implements AsyncRetriever {
    TableTop tableTop;

    AsyncRetrieverImp(TableTop tableTop){
        this.tableTop = tableTop;
    }

    @Override
    public void retrieve() throws IOException {
        String urlString = "https://bitbucket.org/lyonv/ci0161_i2020_examenii/raw/996c22731408d9123feac58627318a7859e82367/Tabletop10.json";
        BufferedReader reader = null;
        try {

            URL url = new URL(urlString);
            reader = new BufferedReader(new InputStreamReader(url.openStream()));
            StringBuffer buffer = new StringBuffer();
            int read;
            char[] chars = new char[1024];
            while ((read = reader.read(chars)) != -1)
                buffer.append(chars, 0, read);

            mapVlues(buffer.toString());

        } catch (JSONException | IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null)
                reader.close();
        };

    }
    private void mapVlues(String json) throws JSONException {
        Map<String, Object> retMap = new Gson().fromJson(json, new TypeToken<HashMap<String, Object>>() {}.getType());
        JSONObject x =new JSONObject(retMap);
        JSONArray recs = x.getJSONArray("miTabletop");
        insertValues(recs);
    }
    private  void  insertValues(JSONArray personas) throws JSONException {
        for (int i = 0; i < personas.length(); ++i) {
            TableTop tableTop = new TableTop();
            JSONObject rec = personas.getJSONObject(i);
            tableTop.setIdentificacion(rec.getString("identificacion"));
            tableTop.setNombre(rec.getString("nombre"));
            tableTop.setPublisher(rec.getString("publisher"));
            tableTop.setYear(rec.getInt("year"));
            this.tableTop.fileName.add(tableTop);
        }
    }

    @Override
    protected Void doInBackground(Object... objects) {
        try {
            retrieve();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
