package cql.ecci.ucr.ac.examen02;

import android.content.Context;
import android.os.Handler;

import java.util.List;

public class TableTopInteractorImpl implements TableTopInteractor {
    private ItemsRepository mItemsRepository;

    @Override public void getItems(final OnFinishedListener listener, final Context context) {
        // Enviamos el hilo de ejecucion con un delay para mostar la barra de progreso
        new Handler().postDelayed(new Runnable() {
            @Override public void run() {

                List<TableTop> items = null;
                mItemsRepository = new ItemsRepositoryImp();
                try {

                    // obtenemos los items
                    items = mItemsRepository.obtainItems(context);

                } catch (CantRetrieveItemsException e) {
                    e.printStackTrace();
                }

                // Al finalizar retornamos los items
                listener.onFinished(items);
            }
        },100);
    }
}

