package cql.ecci.ucr.ac.examen02;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class TableTop implements Parcelable {
    private String identificacion;
    private String nombre;
    private Integer year;
    private String publisher;

    public List<TableTop> fileName = new ArrayList<>();
    public AsyncTask<?, ?, ?> runningTask;

    public TableTop (String identificacion, String nombre, Integer year, String publisher) {
        this.setIdentificacion(identificacion);
        this.setNombre(nombre);
        this.setYear(year);
        this.setPublisher(publisher);
    }
    public TableTop(){}

    protected TableTop(Parcel in) {
        setIdentificacion(in.readString());
        setNombre(in.readString());
        if (in.readByte() == 0) {
            setYear(null);
        } else {
            setYear(in.readInt());
        }
        setPublisher(in.readString());
        setFileName(in.createTypedArrayList(TableTop.getCREATOR()));
    }

    private static final Creator<TableTop> CREATOR = new Creator<TableTop>() {
        @Override
        public TableTop createFromParcel(Parcel in) {
            return new TableTop(in);
        }

        @Override
        public TableTop[] newArray(int size) {
            return new TableTop[size];
        }
    };

    public static Creator<TableTop> getCREATOR() {
        return CREATOR;
    }

    public void leer (Context context) throws ExecutionException, InterruptedException {
        runningTask = new AsyncRetrieverImp(this);
        runningTask.execute().get();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(getIdentificacion());
        dest.writeString(getNombre());
        if (getYear() == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(getYear());
        }
        dest.writeString(getPublisher());
        dest.writeTypedList(getFileName());
    }


    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public List<TableTop> getFileName() {
        return fileName;
    }

    public void setFileName(List<TableTop> fileName) {
        this.fileName = fileName;
    }

    public AsyncTask<?, ?, ?> getRunningTask() {
        return runningTask;
    }

    public void setRunningTask(AsyncTask<?, ?, ?> runningTask) {
        this.runningTask = runningTask;
    }
}
