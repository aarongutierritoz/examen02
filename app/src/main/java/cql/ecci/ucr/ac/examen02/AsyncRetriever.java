package cql.ecci.ucr.ac.examen02;

import java.io.IOException;

public interface AsyncRetriever {
    void retrieve() throws IOException;
}