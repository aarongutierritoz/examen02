package cql.ecci.ucr.ac.examen02;

public class CantRetrieveItemsException extends Exception {

    public CantRetrieveItemsException(String msg) {
        super(msg);
    }
}