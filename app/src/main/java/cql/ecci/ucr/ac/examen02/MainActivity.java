package cql.ecci.ucr.ac.examen02;

import android.content.Context;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;

import java.util.ArrayList;
import java.util.List;


//    Este ejemplo realiza una implementacion de una app con el patron MVP y utilizando lasrecomendaciones de Clean Architecture
//    Se implementa con interfaces para mantener las clases desacopladas entre capas
//    La app muestra al usuario una lista de elementos

//    MainActivity es la actividad principal de la app he implementa la interfaceMainActivityView
//    View (V) llama a Presenter (P) cuando se da una interaccion  de usuario
//    La implementacion de Presenter (P) llama a la clase de negocio GetListItemsInteractor (P)para obtener los resultados,
//       en este caso la lista de elementos a mostrar
//    La implementacion de GetListItemsInteractor (P) retorna los resultados y devuelve elcontrol a la implementacion de Presenter (P)
//       Interactor utiliza las clases de la capa de datos para obtener los items
//    La implementacion de Presenter (P) llama a los metodos del View (V) para actualizar la UIpor medio de su interface
//             Las interfaces de View, Presenter, Interactor y Listener son utilizadas paralograr el desacoplamiento

// Capa de presentacion (Vista)
public class MainActivity extends FragmentActivity implements
        // interface View (V) para implementar los metodos de UI
        MainActivityView,
        // interface para implementar el listener del metodo onItemClick de la lista
        AdapterView.OnItemClickListener {

    private ListView mListView;
    private ProgressBar mProgressBar;
    private MainActivityPresenter mMainActivityPresenter;
    private List<TableTop> list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mListView = (ListView) findViewById(R.id.list);

        mProgressBar = (ProgressBar) findViewById(R.id.progress);

        // Llamada al Presenter
        mMainActivityPresenter = new MainActivityPresenterImp(this, getApplicationContext());
    }

    @Override protected void onResume() {
        super.onResume();
        mMainActivityPresenter.onResume();
    }

    @Override public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override protected void onDestroy() {
        mMainActivityPresenter.onDestroy();
        super.onDestroy();
    }

    // Mostrar el progreso en la UI del avance de la tarea a realizar
    @Override public void showProgress() {
        mProgressBar.setVisibility(View.VISIBLE);
        mListView.setVisibility(View.INVISIBLE);
    }

    // Esconder el indicador de progreso de la UI
    @Override public void hideProgress() {
        mProgressBar.setVisibility(View.INVISIBLE);
        mListView.setVisibility(View.VISIBLE);
    }

    // Mostrar los items de la lista en la UI
    //    Con la lista de items muestra la lista
    @Override public void setItems(List<TableTop> items) {
        //mListView.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_list_item_1,items));

        LazyAdapter mAdapter = new LazyAdapter(items,this);
        mListView.setAdapter(mAdapter);
        mListView.setOnItemClickListener(this);
        if(items != null)
            this.list = new ArrayList<TableTop>(items);
        else{
            this.list = new ArrayList<TableTop>();
        }
    }

    // Mostrar mensaje en la UI
    @Override public void showMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    // Evento al dar clic en la lista
    @Override public void onItemClick(AdapterView<?> parent, View view, int position, long id)
    {
        mMainActivityPresenter.onItemClicked(position, list);
        TableTop selectedData = list.get(position);

        DetailFragment detailFragment = new DetailFragment();

        // Se utiliza bundle para 'encapsular' el objeto
        Bundle bundle = new Bundle();

        // Se pasa el objeto como parámetro al fragmento
        // Esto se logra por medio de Parcelable
        bundle.putParcelable("selected_data", selectedData);
        detailFragment.setArguments(bundle);

        FragmentManager manager = getSupportFragmentManager();
        manager.beginTransaction().replace(R.id.detail, detailFragment).commit();
    }
}
