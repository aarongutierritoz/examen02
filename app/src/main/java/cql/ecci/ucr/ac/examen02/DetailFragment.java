package cql.ecci.ucr.ac.examen02;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

public class DetailFragment extends Fragment {

    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER

    TableTop tableTop;
    public DetailFragment() {
        // Required empty public constructor
    }


    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_detail, container, false);
        TextView tableTopDetail = (TextView)view.findViewById(R.id.detail);

        // Se 'desencapsula' el objeto seleccionado recibido desde la activida principal
        tableTop = (TableTop) getArguments().getParcelable("selected_data");

        // Revisa si este no está vacío
        if (tableTop != null) {

            // Asigna al componente del layout el valor correspondiete de descripción completa
            tableTopDetail.setText("Detalles de " +tableTop.getNombre()+"\n" +
                    "Publisher: " + tableTop.getPublisher()+"\n" +
                    "Año de publicacion: "+ tableTop.getYear()+"\n" +
                    "Codigo de juego: "+tableTop.getIdentificacion());

        }

        return view;

    }
}
