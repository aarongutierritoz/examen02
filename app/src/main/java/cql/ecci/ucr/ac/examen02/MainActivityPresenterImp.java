package cql.ecci.ucr.ac.examen02;

import android.content.Context;
import android.widget.ListView;

import androidx.fragment.app.FragmentActivity;

import java.util.List;

public class MainActivityPresenterImp extends FragmentActivity implements
        // Implementacion de MainActivityPresenter de la Capa (P)
        MainActivityPresenter,
        // La interface OnFinishedListener de GetListItemsInteractor para obtener el valor deretorno de la capa Iteractor (P)
        TableTopInteractor.OnFinishedListener {

    private MainActivityView mMainActivityView;
    private TableTopInteractorImpl mGetListItemsInteractor;
    private Context context;
    private ListView mListView;

    public MainActivityPresenterImp(MainActivityView mainActivityView, Context context) {
        this.mMainActivityView = mainActivityView;
        this.context = context;
        // Capa de negocios (Interactor)
        this.mGetListItemsInteractor = new TableTopInteractorImpl();

    }

    @Override
    public void onResume() {
        if (mMainActivityView != null) {
            mMainActivityView.showProgress();
        }

        // Obtener los items de la capa de negocios (Interactor)
        mGetListItemsInteractor.getItems(this, context);
    }

    // Evento de clic en la lista
    @Override
    public void onItemClicked(int position, List<TableTop> list) {
        if (mMainActivityView != null) {
            mMainActivityView.showMessage(String.format("Position %d clicked", position + 1));

        }
    }

    @Override
    public void onDestroy() {
        mMainActivityView = null;
    }

    @Override
    public void onFinished(List<TableTop> items) {
        if (mMainActivityView != null) {

            mMainActivityView.setItems(items);

            mMainActivityView.hideProgress();
        }
    }

    // Retornar la vista
    public MainActivityView getMainView() {
        return mMainActivityView;
    }
}
