package cql.ecci.ucr.ac.examen02;

import android.content.Context;

import java.util.List;

public interface TableTopInteractor {
    interface OnFinishedListener {
        void onFinished(List<TableTop> items);
    }

    void getItems(OnFinishedListener listener, Context context);
}
