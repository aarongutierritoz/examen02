package cql.ecci.ucr.ac.examen02;

import android.content.Context;

import java.util.List;

public interface ItemsRepository {
    List<TableTop> obtainItems(Context context) throws CantRetrieveItemsException;
}
