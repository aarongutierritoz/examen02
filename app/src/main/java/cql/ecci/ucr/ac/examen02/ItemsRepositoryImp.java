package cql.ecci.ucr.ac.examen02;

import android.content.Context;

import java.util.List;

public class ItemsRepositoryImp implements ItemsRepository
{
    private IServiceDataSource mDataBaseDataSource;
    @Override
    public List<TableTop> obtainItems(Context context) throws CantRetrieveItemsException {
        List<TableTop> items = null;
        // Se realizan las llamadas a las fuentes de datos de donde se obtienen los datos
        //    Ejemplo: base de datos local, archivos locales, archivos en la red, bases de datos en la red

        try {

            mDataBaseDataSource = new IServiceDataSourceImp();
            items = mDataBaseDataSource.obtainItems(context);

        } catch (BaseDataItemsException e) {
            throw new CantRetrieveItemsException(e.getMessage());
        }

        return items;
    }
}
