package cql.ecci.ucr.ac.examen02;

import java.util.List;

public interface MainActivityPresenter {
    // resumir
    void onResume();

    // evento cuando se hace clic en la lista de elementos
    void onItemClicked(int position, List<TableTop> p);

    // destruir
    void onDestroy();
}
