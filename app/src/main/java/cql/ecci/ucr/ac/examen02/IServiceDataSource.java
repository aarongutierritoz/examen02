package cql.ecci.ucr.ac.examen02;

import android.content.Context;

import java.util.List;
public interface IServiceDataSource {
    List<TableTop> obtainItems (Context context) throws BaseDataItemsException;
}
