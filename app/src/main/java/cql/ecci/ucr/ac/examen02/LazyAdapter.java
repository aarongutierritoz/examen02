package cql.ecci.ucr.ac.examen02;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

public class LazyAdapter extends BaseAdapter {
    private List<TableTop> mData;
    private Context mContext;
    public LazyAdapter(List<TableTop> data, Context context) {
        mData = data;
        mContext = context;
    }
    public int getCount() {
        if(mData == null)
            return 0;
        return mData.size();
    }
    public Object getItem(int position) { return position;
    }
    public long getItemId(int position) { return position;
    }
    public View getView(int position, View convertView, ViewGroup parent) { //
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = null; //
        TableTop mEstudiante = mData.get(position);
//
        rowView = inflater.inflate(R.layout.list_row, parent, false); //
        TextView nombre = (TextView)rowView.findViewById(R.id.nombre);
        TextView carne = (TextView)rowView.findViewById(R.id.carne);
        //ImageView imagen = (ImageView)rowView.findViewById(R.id.imagen);

        nombre.setText(mEstudiante.getNombre());
        carne.setText("By "+ mEstudiante.getPublisher());
        //imagen.setImageResource(mEstudiante.getIdImagen());

        return rowView;
    }
}